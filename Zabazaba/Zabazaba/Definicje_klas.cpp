#include <iostream>
#include "Deklaracje_klas.h"
#include <deque>
#include <vector>
#include <conio.h>//co to?
#include <Windows.h> //granie dzwieku
#include <time.h>//do losowania w czasie
using namespace std;


cZaba::cZaba(int szerokosc)
{
	x = (szerokosc / 2);
	y = 0;
};

void cZaba::WyswietlOpis()
{
	cout << "To jest zaba. Bedziesz dzisiaj pomagac przejsc jej bezpiecznie przez rzeke :)" << endl;
	cout << "Z" << endl;
	cout << "Postaraj sie, zeby zaba nie zamoczyla lapek, bedzie to oznaczac koniec gry!" << endl;
	cout << "Skacz bezpiecznie po klodach, jednak uwazaj! Zaba nie plynie razem z klodami, mokre drewno wyslizguje sie spod lapek :)" << endl;
	cout << endl;
};

cWoda::cWoda(int szerokosc)
{
	for (int i = 0; i < szerokosc; i++)
		woda.push_front(true); //na poczatku jest sama woda na planszy, push front dodaje elementy do listy na przodzie listy
	prawo = rand() % 2;
};

void cWoda::Ruch() //tu jaki blad
{
	/**if (prawo)
	{
	if (rand() % 10 == 1)
		woda.push_front(true); //jesli jest 1 to dodamy klode do kolejki
	else
		woda.push_front(false); //nie dodamy wody
	woda.pop_back(); //ostatni element kolejki jest usuwany, zeby zachowac te sama ilosc elementow po przesunieciu kolejk
	}
	else
	{
	if (rand() % 10 == 1)
		woda.push_front(true); //jesli jest 1 to dodamy klode do kolejki
	else
		woda.push_back(false); //nie dodamy wody
	woda.pop_front(); //pierwszy element kolejki jest usuwany, zeby zachowac te sama ilosc elementow po przesunieciu kolejk
	} **/
	if (prawo)
	{
		if (rand() % 10 == 1)
			woda.push_front(true);
		else
			woda.push_front(false);
		woda.pop_back();
	}
	else
	{
		if (rand() % 10 == 1)
			woda.push_back(true);
		else
			woda.push_back(false);
		woda.pop_front();
	}

};

bool cWoda::SprawdzPozycje(int pozycja)
{
	return woda[pozycja]; //jesli na pozycji o danym numerze (np 1 albo 3) bedzie znajdowac sie kloda to funkcja zwroci true, jesli nie bedzie klody to false
};

void cWoda::ZmianaKierunku()
{
	prawo = !prawo;
};

void cWoda::WyswietlOpis() {
	cout << "Klody plynace przez wode wygladaja tak ja ponizej:" << endl;
	cout << "~" << endl;
	cout << "Zeby przejsc na druga strone rzeki musisz skakac tylko po nich! Nie wpadnij do wody :) Woda to puste pola na planszy" << endl;
};


cGra::cGra(int w, int h)//ustawienia startowe przy wywo�aniu gry
{
	liczbarzedow = h;
	szerokosc = w;
	zakoncz = false; //gra caly czas ma dzialac od poczatku
	for (int i = 0; i < liczbarzedow; i++)
		mapa.push_back(new cWoda(szerokosc));//wskaznik, ktory zaalokuje pamiec dla nowej kolejki klod
	zaba = new cZaba(szerokosc); //zobaczyc co to znaczy, chyba wskaznik, stworzenie wskaznika na gracza?
};

cGra::~cGra()
{
	delete zaba;
	for (int i = 0; i < mapa.size(); i++)
	{
		cWoda* current = mapa.back();
		mapa.pop_back();
		delete current;
	}
};

void cGra::Rysuj()
{
	system("cls");
	for (int i = 0; i < liczbarzedow; i++)//przejscie przez wszystkie rzedy
	{
		for (int j = 0; j < szerokosc; j++)//przejscie przez wszystkie kolumny
		{
			if (mapa[i]->SprawdzPozycje(j) && i != 0 && i != liczbarzedow - 1)//je�li mapa w tym miejscu jest ustawiona to sprawdz pozycje, omi� pierwszy i ostatni rzad - tam parkuje zaba (minus 1 bo liczymy od 0 
				cout << " "; //je�li w tym polu znajduje sie woda to narysujemy na mapie puste pole
			else if (zaba->x == j && zaba->y == i) //je�li zaba znajduje sie na omawianym polu to
				cout << "Z"; //narysuj znak zaby "Z"
			else
				cout << "~";//narysuj tylde czyli, klode, jesli w danym miejscu nie ma ani wody ani zaby			
		}
		cout << endl;
	}
	cout << "Wynik: " << wynik << endl;
};

void cGra::Zmienne()//zrob zabezpieczania, zeby nic sie nie stalo, jesli klikniemy jakies inne dziwne przyciski
{
	if (_kbhit()) //sprawdza czy uderzenie klawisza jest mozliwe
	{
		char current = _getch(); //jesli wcisniecie klawisza jest mozliwe to przechwyc nacisniety nim charakter
		if (current == 'a')
			zaba->x--; //jesli gracz nacisnie "a" to przesuwamy zabe o jedno pole w lewo
		if (current == 'd')
			zaba->x++; // podobnie tylko w prawo
		if (current == 'w')
			zaba->y--;
		if (current == 's')
			zaba->y++;
		if (current == 'q') //wychodzimy z gry, jesli gracz nacisnie q
			zakoncz = true;
	}
};

void cGra::Logika()
{
	for (int i = 1; i < liczbarzedow - 1; i++) //zaczynamy od rzedu 1, bo zaba ma stacje dokujaca na 0 i tak samo bez koncowego rzedu, dodaj potem wiecej rzedow!!!
	{
		if (rand() % 10 == 1)
			mapa[i]->Ruch();
		if (mapa[i]->SprawdzPozycje(zaba->x) && zaba->y == i)
		{
			zakoncz = true;
			cout << "\x07"; //puszczenie dzwieku bledu w windowsie po uderzeniu w przeszkode
		}
	}
	if (zaba->y == liczbarzedow - 1)
	{
		wynik++;
		zaba->y = 0;
		mapa[rand() % liczbarzedow]->ZmianaKierunku();//po przejsciu poziomu zmienia kierunek losowego rzedu
		//dodanie muzyczki, ktora zagra jak dostanie sie punkt
	}
};

void cGra::Dzialaj()
{
	while (!zakoncz)
	{
		Zmienne();
		Rysuj();
		Logika();
	}
};

void cGra::WyswietlOpis() {
	cout << "Witaj w grze MINI FROGGER!!! W tej grze masz powazne zadanie - bedziesz eskortowal zabe na druga strone rzeki :)" << endl;
	cout << "Oto kilka podstawowych zasad:" << endl;
	cout << "Poruszaj zaba za pomoca przyciskow W,S,A i D." << endl;
	cout << "Zaba jest bezpieczna na polach klod, kiedy staniesz na polu wody to przegrasz." << endl;
	cout << "Po przejsciu na druga strone rzeki, zaba wraca na jej pierwszy brzeg." << endl;
	cout << "Po kazdym udanym przejsciu otrzymasz punkt, zbieraj punkty i pobij swoj wlasny rekord :D." << endl;
	cout << endl;
};



