#include <iostream>
#include <deque>
#include <vector>
#include <conio.h>
#include <Windows.h>
#include <time.h>
using namespace std;

/**class cObiekt {
protected: //ustawiam protected, zeby funkcje dziedziczace mialy do nich dostep, a jednoczesnie oznajmiam przyjazn z klasa, w ktorej metodzie bede tych
	//zmiennych uzywac
	//i zeby byly nadal private
	int x, y; //polozenie na planszy
public:
	friend class cGra;// z ta klasa przyjazni sie klasa cGra - bedzie mogla uzywac dostepu do atrybutow x i y w swoich metodach
	//virtual void WyswietlanieInstrukcji()=0; - wyswietlenie instrukcji i opisu obiektu przed gra
};**/

class cObiekt {
public:
	virtual void WyswietlOpis() = 0;
};

class cZaba:public cObiekt{ //klasa cZaba dziedziczy publicznie po cObiekt
	int x, y;
public:
	cZaba(int szerokosc); //pozycja startowa zaby jest ustalona,mozna zmienic jej nazwe
	virtual void WyswietlOpis();
	friend class cGra;
	friend class cWoda;
};

class cWoda:public cObiekt{
	deque<bool> woda;
	bool prawo; //do okreslania kierunku poruszania sie klod
public:
	cWoda(int szerokosc);
	void Ruch();//woda porusza sie w czasie o jedno pole w lewo
	bool SprawdzPozycje(int pozycja);
	void ZmianaKierunku();
	virtual void WyswietlOpis();
};

class cGra:public cObiekt {
	bool zakoncz;
	int liczbarzedow;
	int szerokosc;
	int wynik;
	cZaba* zaba; //zainicjowanie wskaznika dla gracza
	vector<cWoda*> mapa; //sprawdzic jak dziala wektor i wskaznik

public:
	cGra(int w = 20, int h = 10);//konstruktor, szerokosc, wysokosc, 
	~cGra();
	void Rysuj(); //rysuje na planszy albo wode albo klode 
	void Zmienne();
	void Logika();//pojawianie zaby na poczatku gry i po zniknieciu, znikanie zaby kiedy trafi na klocek wody, plyniecie klockow wody i klody,
	//losowanie czy pojawi sie woda czy kloda, rozmieszczenie ich w ilus liniach, definiowanie kiedy zaba traci punkt zycia, dodanie punktow dla gracza
	//po udanym przejsciu, zliczanie punktow, wyjscie z gry, wyswietlenie informacji poczatkowych, moliwosc nazwania zaby, wywolywanie wskaznikiem odpowiednich
	//metod klas
	void Dzialaj();//program dziala jesli wartosc zakoncz nie jest prawdziwa
	virtual void WyswietlOpis();
};